# Salesforce metaChecker utility #

### A quick, lightweight way to check for and list changes to setup metadata within a Salesforce org. ###

* leverages the awesome [salesforce-metadata.js](https://github.com/mino0123/salesforce-metadata.js)

### Version History ###
* 0.1 initial release
* 0.2 added ability to quickly export changes as package.xml file, for use with ANT deployments.

### Why would I use this ? ###

Some Salesforce development projects are relatively straight forward and don't warrant complex backups and/or continuous integration solutions. You're writing some new code, tweaking some settings and you're pushing a change set or two.  That said, the manual documentation of changes that you want to package up seems a little prone to human error, or you just want to double check you captured everything.  This is where metaChecker might prove useful. Built as a single vf page with no dependant components, the idea is to give you the developer a quick and simple way to sanity check that you've captured all metadata changes into that change set bound for production. Note that it wont capture destructive changes you've made, since the metadata isn't there any more.  You'd need to do a diff against a baseline snapshot to find those (hmmm maybe next util...)

You may also not enjoy creating package.xml manifests, so I've popped that in as well.

### How do I get set up? ###

* Simple. just drop the page into any Salesforce org you want to look at. The utility is contained within a single Visualforce page.

### How do I use it ? ###

Usage is straightforward.

* just navigate to the page itself. Typically, /apex/metaChecker.  The page on load will retrieve metadata types that it can search across.

![Screen Shot 2015-05-04 at 4.48.27 pm.png](https://bitbucket.org/repo/qeEoAk/images/547005986-Screen%20Shot%202015-05-04%20at%204.48.27%20pm.png)

* Select which metadata objects you want to search. The select panel is multi-select.

![Screen Shot 2015-05-04 at 4.51.38 pm.png](https://bitbucket.org/repo/qeEoAk/images/2443217662-Screen%20Shot%202015-05-04%20at%204.51.38%20pm.png)

* Default behaviour is to pull in all metadata. If you want to filter results to only metadata that has been added or changed between now and a specific date and time, populate the input field provided. Use YYYY-MM-DDTHH:MM:SSZ format.

![Screen Shot 2015-05-04 at 4.57.19 pm.png](https://bitbucket.org/repo/qeEoAk/images/595107440-Screen%20Shot%202015-05-04%20at%204.57.19%20pm.png)

* Click the 'Search Metadata' button. Results will appear in a scrollable panel at right of screen. Created and Last Modified date information is also provided for reference if filtering.

![Screen Shot 2015-05-04 at 5.00.04 pm.png](https://bitbucket.org/repo/qeEoAk/images/2026381208-Screen%20Shot%202015-05-04%20at%205.00.04%20pm.png)

* Click 'Printable view' button to pop the results into a print friendly format.
* Click 'Package.xml view' button to pop a package.xml covering off listed components into a new window.

### Contact details ###

* carl at truecloud.com.au 
* https://au.linkedin.com/in/carlvescovi